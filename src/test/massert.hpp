#define massert(x, s, ...) _massert(x, __FILE__, __LINE__, s, __VA_ARGS__)

int _massert(int x, const char *file, int line, const char *s, ...);