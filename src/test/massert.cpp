#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <verilated.h>

extern vluint64_t sim_time;

int _massert(int x, const char *file, int line, const char *s, ...){
	if(!x) fprintf(stderr, "File: %s, line: %d, time: %d >> ", file, line, sim_time);
	va_list args;
	va_start(args,s);
	if(!x) vfprintf(stderr, s, args);
	va_end(args);
	return x;
}