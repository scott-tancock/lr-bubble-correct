#include "Vbin_pack.h"
#include "verilated.h"
#include "verilated_fst_c.h"
#include <iostream>
#include "massert.hpp"
#include <random>
#include <limits>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#define CLK_WAIT (4)
#ifndef CLK_PERIOD
#define CLK_PERIOD (10)
#warning("auto-defining clock period")
#endif

#define CLK_INV (CLK_PERIOD / 2)
#define RESET_TIME (CLK_PERIOD * CLK_WAIT)

#define N_TESTS (100)
#define N_BLOCKS (24)
#define BLOCK_MAX (48)
#define BLOCK_SIZE (6)
#define LEN (BLOCK_SIZE * N_BLOCKS)

vluint64_t sim_time = 0;
typedef unsigned char byte;

std::default_random_engine generator;
uint32_t di_store[N_TESTS][5];
uint32_t do_store[N_TESTS][5];
uint32_t aux_store[N_TESTS][3];
int di_ptr = 0;
int do_ptr = 0;
int aux_ptr = 0;


/*
	input  wire           clk      ,
	input  wire           rst      ,
	input  wire [LEN-1:0] data_in  ,
	input  wire           valid_in ,
	output wire           ready_in ,
	output wire [LEN-1:0] data_out ,
	output wire           valid_out,
	input  wire           ready_out
*/

void di_clear(){
	for(int i = 0; i < N_TESTS; i++){
		di_store[i][0] = 0;
		di_store[i][1] = 0;
		di_store[i][2] = 0;
		di_store[i][3] = 0;
		di_store[i][4] = 0;
	}
	di_ptr = 0;
}

void do_clear(){
	for(int i = 0; i < N_TESTS; i++){
		do_store[i][0] = 0;
		do_store[i][1] = 0;
		do_store[i][2] = 0;
		do_store[i][3] = 0;
		do_store[i][4] = 0;
	}
	do_ptr = 0;
}

void aux_clear(){
	for(int i = 0; i < N_TESTS; i++){
		aux_store[i][0] = 0;
		aux_store[i][1] = 0;
		aux_store[i][2] = 0;
	}
	aux_ptr = 0;
}

void di_log(uint32_t d_i[5]){
	di_store[di_ptr][0] = d_i[0];
	di_store[di_ptr][1] = d_i[1];
	di_store[di_ptr][2] = d_i[2];
	di_store[di_ptr][3] = d_i[3];
	di_store[di_ptr][4] = d_i[4];
	di_ptr++;
}

void do_log(uint32_t d_o[5]){
	do_store[do_ptr][0] = d_o[0];
	do_store[do_ptr][1] = d_o[1];
	do_store[do_ptr][2] = d_o[2];
	do_store[do_ptr][3] = d_o[3];
	do_store[do_ptr][4] = d_o[4];
	do_ptr++;
}

void aux_log(uint32_t aux[2]){
	aux_store[aux_ptr][0] = aux[0];
	aux_store[aux_ptr][1] = aux[1];
	aux_store[aux_ptr][2] = aux[2];
	aux_ptr++;
}

void d_print_old(){
	std::cout << "Results" << std::setbase(8) << std::setfill('0') << std::endl;
	for(int i = 0; i < N_TESTS; i++){
		std::cout << "di:" << std::setw(4) << di_store[i][4] << "_" << std::setw(8) << di_store[i][3] << "_" << std::setw(8) << di_store[i][2] << "_" << std::setw(8) << di_store[i][1] << "_" << std::setw(8) << di_store[i][0] << ", do:" << std::setw(4) << do_store[i][4] << "_" << std::setw(8) << do_store[i][3] << "_" << std::setw(8) << do_store[i][2] << "_" << std::setw(8) << do_store[i][1] << "_" << std::setw(8) << do_store[i][0] << std::endl;
	}
	std::cout << std::setbase(10) << std::setfill(' ');
}

void dx_print_helper(uint32_t d_store[5]){
	//Buffer to compose in
	char compose[N_BLOCKS*3+1];
	//Bit cursor
	int bit_idx = (N_BLOCKS*BLOCK_SIZE)%32;
	//uint32_t (word) cursor
	int wrd_idx = (((N_BLOCKS*BLOCK_SIZE)-1)/32);
	byte d_next;
	int char_idx = 0;
	for(int j = 0; j < N_BLOCKS; j++){
		if(bit_idx == 0){
			bit_idx = 32;
			wrd_idx--;
		}
		bit_idx = bit_idx - BLOCK_SIZE;
		if(bit_idx >= 0){
			d_next = (d_store[wrd_idx] >> bit_idx) & ((1 << BLOCK_SIZE)-1);
		}
		else{
			d_next = d_store[wrd_idx] & ((1 << (bit_idx + BLOCK_SIZE))-1);
			wrd_idx--;
			bit_idx += 32;
			d_next = d_next << (32-bit_idx);
			d_next = d_next | ((d_store[wrd_idx] >> bit_idx) & ((1 << (32-bit_idx))-1));
		}
		char_idx += snprintf(compose+char_idx, N_BLOCKS*3+1-char_idx, "_%02o", d_next);
	}
	std::cout << compose;
}

void d_print(){
	std::cout << "Results" << std::setbase(8) << std::setfill('0') << std::endl;
	for(int i = 0; i < N_TESTS; i++){
		std::cout << std::setbase(16);
		//std::cout << "di:" << std::setw(4) << di_store[i][4] << "_" << std::setw(8) << di_store[i][3] << "_" << std::setw(8) << di_store[i][2] << "_" << std::setw(8) << di_store[i][1] << "_" << std::setw(8) << di_store[i][0] << ", do:" << std::setw(4) << do_store[i][4] << "_" << std::setw(8) << do_store[i][3] << "_" << std::setw(8) << do_store[i][2] << "_" << std::setw(8) << do_store[i][1] << "_" << std::setw(8) << do_store[i][0] << std::endl;
		std::cout << std::setbase(8);
		std::cout << "di:";
		dx_print_helper(di_store[i]);
		std::cout << ", do:";
		dx_print_helper(do_store[i]);
		std::cout << std::setbase(16) << ", aux:" << aux_store[i][2] << "_" << aux_store[i][1] << "_" << aux_store[i][0] << std::setbase(8);
		std::cout << std::endl;
	}
	std::cout << std::setbase(10) << std::setfill(' ');
}

void di_randomise(uint32_t di[5]){
	std::uniform_int_distribution<int> val_gen(0,BLOCK_MAX-1);
	std::uniform_int_distribution<int> idx_gen(0,2);
	std::uniform_int_distribution<int> side_gen(0,1);
	int flip_idxs[N_BLOCKS/6];
	bool flip[N_BLOCKS];
	bool side[N_BLOCKS][2];
	int vals[N_BLOCKS];
	int exp = 48;
	int bit_acc = 0;
	uint32_t load_val = 0;
	int load_idx = 0;
	for(int i = 0; i < N_BLOCKS; i++){
		flip[i] = false;
		side[i][0] = false;
		side[i][1] = false;
	}
	for(int i = 0; i < N_BLOCKS/6; i++){
		flip_idxs[i] = idx_gen(generator);
		flip[i*6+flip_idxs[i]] = true;
		side[i*6+flip_idxs[i]][0] = (bool)side_gen(generator);
		side[i*6+flip_idxs[i]][1] = (bool)side_gen(generator);
	}
	std::cout << "Load vals: " << std::setbase(16) << std::setfill('0');
	for(int i = N_BLOCKS-1; i >= 0; i--){
		if(flip[i]){
			vals[i] = val_gen(generator);
			exp = BLOCK_MAX - exp;
		}
		else if(i < N_BLOCKS-1 && flip[i+1] && side[i+1][0]){
			vals[i] = val_gen(generator);
		}
		else if(i > 0 && flip[i-1] && side[i-1][1]){
			vals[i] = val_gen(generator);
		}
		else {
			vals[i] = exp;
		}
		std::cout << std::setw(2) << vals[i] << " ";
	}
	std::cout << std::setbase(10) << std::setfill(' ') << std::endl;
	for(int i = 0; i < N_BLOCKS; i++){
		if(bit_acc + BLOCK_SIZE > 32){
			//load_val = (load_val << (32 - bit_acc)) | vals[i] & ((1 << (32 - bit_acc))-1);
			load_val = load_val | ((vals[i] & ((1 << (32-bit_acc))-1)) << bit_acc);
			bit_acc += BLOCK_SIZE;
		}
		else {
			//load_val = (load_val << BLOCK_SIZE) | vals[i];
			load_val = load_val | (vals[i] << bit_acc);
			bit_acc += BLOCK_SIZE;
		}
		if(bit_acc >= 32){
			di[load_idx++] = load_val;
			load_val = 0;
			bit_acc = bit_acc - 32;
			if(bit_acc){
				load_val = vals[i] >> (BLOCK_SIZE - bit_acc);
			}
		}
	}
	if(bit_acc){
		di[load_idx++] = load_val;
	}
	std::cout << "Randomised data_in: 0x" << std::setbase(16) << std::setfill('0') << std::setw(4) << di[4] << "_" << std::setw(8) << di[3] << "_" << std::setw(8) << di[2] << "_" << std::setw(8) << di[1] << "_" << std::setw(8) << di[0] << std::setbase(10) << std::setfill(' ') << std::endl;
}

bool d_check(){
	bool output = true;
	for(int i = 0; i < N_TESTS; i++){
		massert(aux_store[i][0] == i+1, "Error: item %d has wrong sequence number %d, should be %d\n", i, aux_store[i][0], i+1);
		massert(aux_store[i][1] == 0xCAFEBABE, "Error: item %d has wrong test field %x, should be %x\n", i, aux_store[i][1], 0xCAFEBABE);
		massert(aux_store[i][2] == 0-(i+1), "Error: item %d has wrong reverse-sequence number %d, should be %d", i, *((int *)(aux_store[i]+2)), 0-(i+1));
		uint32_t *id = di_store[i];
		uint32_t *od = do_store[i];
		int exp = BLOCK_MAX;
		uint16_t ide[N_BLOCKS];
		uint16_t ode[N_BLOCKS];
		int wrd_idx = 0;
		int bit_idx = 0;
		for(int j = 0; j < N_BLOCKS; j++){
			if(bit_idx + BLOCK_SIZE < 32){
				ide[j] = (id[wrd_idx] >> bit_idx) & ((1 << BLOCK_SIZE)-1);
				ode[j] = (od[wrd_idx] >> bit_idx) & ((1 << BLOCK_SIZE)-1);
				bit_idx += BLOCK_SIZE;
			}
			else if(bit_idx + BLOCK_SIZE == 32){
				ide[j] = (id[wrd_idx] >> bit_idx) & ((1 << BLOCK_SIZE)-1);
				ode[j] = (od[wrd_idx] >> bit_idx) & ((1 << BLOCK_SIZE)-1);
				bit_idx = 0;
				wrd_idx += 1;
			}
			else{
				ide[j] = (id[wrd_idx] >> bit_idx) & ((1 << (32-bit_idx))-1);
				ode[j] = (od[wrd_idx] >> bit_idx) & ((1 << (32-bit_idx))-1);
				bit_idx += BLOCK_SIZE-32;
				wrd_idx += 1;
				ide[j] |= (id[wrd_idx] & ((1 << bit_idx)-1)) << (BLOCK_SIZE-bit_idx);
				ode[j] |= (od[wrd_idx] & ((1 << bit_idx)-1)) << (BLOCK_SIZE-bit_idx);
			}
		}
		uint16_t cprs[N_BLOCKS];
		int blk_idx = 0;
		for(int j = 1; j < N_BLOCKS; j++){
			//ide same as exp, run of 1s or 0s
			if(ide[j] == exp){
				cprs[j] = exp;
				blk_idx = j;
			}
			//ide opposite to exp, passed a transition point entirely, need to invert exp.
			else if((ide[j] ^ BLOCK_MAX) == exp){
				exp = exp ^ BLOCK_MAX;
				cprs[j] = exp;
				blk_idx = j;
			}
			//part-way through a transition
			else{
				uint16_t pullback = ide[j] + ide[blk_idx];
				uint16_t pb_sub = pullback - BLOCK_MAX;
				//Coming from a run of 1s, pull back
				if(exp == BLOCK_MAX){
					//Sum of look-ahead and current > max, push current forward
					if(pullback >= BLOCK_MAX){
						cprs[blk_idx] = BLOCK_MAX;
						cprs[j] = 0;
						cprs[blk_idx + 1] = pb_sub;
						blk_idx++;
					}
					//Sum < max, leave current where it is.
					else{
						cprs[blk_idx] = pullback;
						cprs[j] = 0;
					}
				}
				//Coming from a run of 0s, push forward
				else{
					//Sum > max
					if(pullback >= BLOCK_MAX){
						cprs[blk_idx] = pb_sub;
						cprs[j] = BLOCK_MAX;
					}
					//Sum < max
					else{
						cprs[blk_idx] = 0;
						cprs[j] = pullback;

					}
					//Current always moves forward when coming from a run of 0s.
					blk_idx++;
				}
			}
		}
		for(int j = 0; j < N_BLOCKS; j++){
			massert(cprs[j] == ode[j], "Error: Invalid output (%d,%d).  Extracted input: %02o, Compressed input: %02o, Extracted output: %02o\n", i, j, ide[j], cprs[j], ode[j]);
			output = false;
		}
	}
	return output;
}

int main(int argc, char **argv, char **env){
	Verilated::commandArgs(argc, argv);
	Vbin_pack *top = new Vbin_pack;
	Verilated::traceEverOn(true);
	VerilatedFstC *trace = new VerilatedFstC;
	top->trace(trace, 99);
	trace->open("wf/bin_pack.fst");
	top->rst = 1;
	top->clk = 0;
	// 24 * 6 = 144 bits = 4.5*uint32_t
	top->data_in[0] = 0;
	top->data_in[1] = 0;
	top->data_in[2] = 0;
	top->data_in[3] = 0;
	top->data_in[4] = 0;
	top->valid_in = 0;
	top->ready_out = 1;
	top->aux_in[0] = 0xDEADBEEF;
	top->aux_in[1] = 0xDEADBABE;
	top->aux_in[2] = 0xBEEFBABE;
	top->eval();
	trace->dump(sim_time);
	vluint64_t old_time = sim_time;
	for(int i = 0; i < 2*CLK_WAIT; i++){
		sim_time += CLK_INV;
		top->clk = !top->clk;
		top->eval();
		trace->dump(sim_time);
	}
	top->rst = 0;
	top->eval();
	trace->dump(sim_time);
	bool do0 = top->data_out[0] == 0 && top->data_out[1] == 0 && top->data_out[2] == 0 && top->data_out[3] == 0 && top->data_out[4] == 0;
	massert(do0, "data_out should be 144'h0, instead, it is 0x%032x%032x%032x%032x%032x\n", top->data_out[4], top->data_out[3], top->data_out[2], top->data_out[1], top->data_out[0]);
	top->valid_in = 1;
	di_clear();
	do_clear();
	aux_clear();
	for(int t = 0; t < N_TESTS; t++){
		// Set up inputs
		top->aux_in[0] = t+1;
		top->aux_in[1] = 0xCAFEBABE;
		top->aux_in[2] = 0-(t+1);
		di_randomise(top->data_in);
		di_log(top->data_in);
		// Wait for 2 clock inversions
		for(int i = 0; i < 2; i++){
			sim_time += CLK_INV;
			top->clk = !top->clk;
			top->eval();
			trace->dump(sim_time);
		}
		if(top->valid_out){
			do_log(top->data_out);
			aux_log(top->aux_out);
		}
	}
	top->valid_in = 0;
	for(int i = 0; i < 64; i++){
		for(int j = 0; j < 2; j++){
			sim_time += CLK_INV;
			top->clk = !top->clk;
			top->eval();
			trace->dump(sim_time);
		}
		if(top->valid_out){
			do_log(top->data_out);
			aux_log(top->aux_out);
		}
		else{
			break;
		}
	}
	d_print();
	d_check();
	top->final();
	trace->dump(sim_time);
	trace->flush();
	trace->close();
	trace = NULL;
	delete top;
	exit(0);
}