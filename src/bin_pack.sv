`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 21.06.2019 12:59:07
// Design Name:
// Module Name: bin_pack
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module bin_pack #(
	parameter N_BLOCKS   = 24                   ,
	parameter BLOCK_SIZE = 6                    ,
	parameter BLOCK_MAX  = 48                   ,
	parameter LEN        = N_BLOCKS * BLOCK_SIZE,
	parameter W_BYTES    = 30                   ,
	parameter WIDTH      = 8*W_BYTES            ,
	parameter AUX        = WIDTH-LEN            ,
	parameter W_BLOCKS   = 8
) (
	input  wire           clk      ,
	input  wire           rst      ,
	input  wire [LEN-1:0] data_in  ,
	input  wire [AUX-1:0] aux_in   ,
	input  wire           valid_in ,
	output wire           ready_in ,
	output wire [LEN-1:0] data_out ,
	output wire [AUX-1:0] aux_out  ,
	output wire           valid_out,
	input  wire           ready_out
);

	wire [     LEN-1:0] interm_data[   N_BLOCKS:0]     ;
	wire [W_BLOCKS-1:0] interm_base[   N_BLOCKS:0]     ;
	/* verilator lint_off UNUSED */
	wire [W_BLOCKS-1:0] skip       [   N_BLOCKS:0]     ;
	/* verilator lint_on UNUSED */
	wire                exp        [   N_BLOCKS:0]     ;
	(* max_fanout = 50 *)
	reg                 occupied   [  N_BLOCKS:-1][1:0];
	reg  [     AUX-1:0] aux        [N_BLOCKS-1:-1][1:0];
	/* verilator lint_off UNOPTFLAT */
	reg en[N_BLOCKS:0][1:0];
	/* verilator lint_on UNOPTFLAT */

	assign interm_data[0] = data_in;
	assign skip[0]        = 1;
	assign interm_base[0] = 0;
	assign data_out       = interm_data[N_BLOCKS];
	assign valid_out      = occupied[N_BLOCKS-1][1];
	assign ready_in       = en[0][0] || !occupied[0][0];
	assign exp[0]         = 1;
	assign aux_out        = aux[N_BLOCKS-1][1];

	always_comb begin : occupied_first
		occupied[-1][0] = valid_in;
		occupied[-1][1] = valid_in;
	end
	always_comb begin : proc_occupied_last
		occupied[N_BLOCKS][0] = !ready_out;
		occupied[N_BLOCKS][1] = !ready_out;
	end
	always_comb begin : proc_en_final
		en[N_BLOCKS][0] = ready_out;
		en[N_BLOCKS][1] = ready_out;
	end
	always_comb begin : proc_aux_first
		aux[-1][0] = aux_in;
		aux[-1][1] = aux_in;
	end

	genvar g_pipe;
	generate
		for(g_pipe = 0; g_pipe < N_BLOCKS; g_pipe++) begin : gen_pipeline
			reg           occupied_next[1:0];
			reg [AUX-1:0] aux_next     [1:0];
			bin_pack_block #(
				.N_BLOCKS  (N_BLOCKS  ),
				.BLOCK_SIZE(BLOCK_SIZE),
				.BLOCK_MAX (BLOCK_MAX ),
				.W_BLOCKS  (W_BLOCKS  )
			) bpb (
				.clk     (clk                  ),
				.rst     (rst                  ),
				.en      (en[g_pipe][0]        ),
				.en2     (en[g_pipe][1]        ),
				.data_in (interm_data[g_pipe]  ),
				.base_in (interm_base[g_pipe]  ),
				.skip_in (g_pipe+1             ),
				.exp_in  (exp[g_pipe]          ),
				.data_out(interm_data[g_pipe+1]),
				.base_out(interm_base[g_pipe+1]),
				.skip_out(skip[g_pipe+1]       ),
				.exp_out (exp[g_pipe+1]        )
			);

			always_comb begin : proc_en
				/* verilator lint_off ALWCOMBORDER */
				en[g_pipe][1] = !occupied[g_pipe+1][0] || en[g_pipe+1][0];
				en[g_pipe][0] = !occupied[g_pipe][1] || en[g_pipe][1];
				/* verilator lint_on ALWCOMBORDER */
			end

			always_comb begin : proc_occupied_next
				occupied_next[0] = (en[g_pipe][0])?occupied[g_pipe-1][1]:occupied[g_pipe][0];
				occupied_next[1] = (en[g_pipe][1])?occupied[g_pipe][0]:occupied[g_pipe][1];
			end

			always_comb begin : proc_aux_next
				aux_next[0] = (en[g_pipe][0])?aux[g_pipe-1][1]:aux[g_pipe][0];
				aux_next[1] = (en[g_pipe][1])?aux[g_pipe][0]:aux[g_pipe][1];
			end

			always_ff @(posedge clk) begin : proc_occupied_g_pipe
				if(rst) begin
					occupied[g_pipe][0] <= 0;
					occupied[g_pipe][1] <= 0;
				end else begin
					occupied[g_pipe][0] <= occupied_next[0];
					occupied[g_pipe][1] <= occupied_next[1];
				end
			end

			always_ff @(posedge clk) begin : proc_aux_g_pipe
				if(rst) begin
					aux[g_pipe][0] <= 0;
					aux[g_pipe][1] <= 0;
				end else begin
					aux[g_pipe][0] <= aux_next[0];
					aux[g_pipe][1] <= aux_next[1];
				end
			end
		end
	endgenerate

endmodule
