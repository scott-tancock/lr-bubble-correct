`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 21.06.2019 13:10:21
// Design Name:
// Module Name: bin_pack_block
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module bin_pack_block #(
	parameter N_BLOCKS   = 24                   ,
	parameter BLOCK_SIZE = 6                    ,
	parameter BLOCK_MAX  = 48                   ,
	parameter LEN        = N_BLOCKS * BLOCK_SIZE,
	parameter W_BLOCKS   = 8
) (
	input  wire                clk     ,
	input  wire                rst     ,
	input  wire                en      ,
	input  wire                en2     ,
	input  wire [     LEN-1:0] data_in ,
	input  wire [W_BLOCKS-1:0] base_in ,
	input  wire [W_BLOCKS-1:0] skip_in ,
	input  wire                exp_in  ,
	(* max_fanout = 25 *)
	output reg  [     LEN-1:0] data_out,
	(* max_fanout = 25 *)
	output reg  [W_BLOCKS-1:0] base_out,
	(* max_fanout = 25 *)
	output reg  [W_BLOCKS-1:0] skip_out,
	(* max_fanout = 25 *)
	output reg                 exp_out
);
	(* max_fanout = 25 *)
	reg [     LEN-1:0] data_out_next;
	(* max_fanout = 25 *)
	reg [W_BLOCKS-1:0] base_out_next;
	(* max_fanout = 25 *)
	reg [W_BLOCKS-1:0] skip_out_next;
	(* max_fanout = 25 *)
	reg                exp_out_next ;

	(* max_fanout = 25 *)
	reg [BLOCK_SIZE-1:0] data_base;
	(* max_fanout = 25 *)
	reg [BLOCK_SIZE-1:0] data_base2;
	assign data_base = data_in[base_in*BLOCK_SIZE +: BLOCK_SIZE];
	(* max_fanout = 25 *)
	reg [BLOCK_SIZE-1:0] data_skip;
	(* max_fanout = 25 *)
	reg [BLOCK_SIZE-1:0] data_skip2;
	assign data_skip = data_in[skip_in*BLOCK_SIZE +: BLOCK_SIZE];
	(* max_fanout = 25 *)
	reg [BLOCK_SIZE-1:0] data_base_inc;
	(* max_fanout = 25 *)
	reg [BLOCK_SIZE-1:0] data_base_inc2;
	assign data_base_inc = data_in[(base_in+1)*BLOCK_SIZE +: BLOCK_SIZE];
	(* max_fanout = 25 *)
	reg [BLOCK_SIZE:0] pullback2;
	//assign pullback = data_base + data_skip;
	(* max_fanout = 25 *)
	reg [BLOCK_SIZE-1:0] exp2;
	//assign exp = {BLOCK_SIZE{exp_in}} & BLOCK_MAX;
	(* max_fanout = 25 *)
	reg [     LEN-1:0] data_in2;
	(* max_fanout = 25 *)
	reg [W_BLOCKS-1:0] base_in2;
	(* max_fanout = 25 *)
	reg [W_BLOCKS-1:0] skip_in2;
	(* max_fanout = 25 *)
	reg                exp_in2 ;

	always_ff @(posedge clk) begin : proc_data_base2
		if(rst) begin
			data_base2 <= 0;
		end else begin
			data_base2 <= (en)?data_base:data_base2;
		end
	end

	always_ff @(posedge clk) begin : proc_data_skip2
		if(rst) begin
			data_skip2 <= 0;
		end else begin
			data_skip2 <= (en)?data_skip:data_skip2;
		end
	end

	always_ff @(posedge clk) begin : proc_data_base_inc2
		if(rst) begin
			data_base_inc2 <= 0;
		end else begin
			data_base_inc2 <= data_base_inc;
		end
	end

	always_ff @(posedge clk) begin : proc_pullback2
		if(rst) begin
			pullback2 <= 0;
		end else begin
			pullback2 <= (en)?data_base + data_skip:pullback2;
		end
	end

	always_ff @(posedge clk) begin : proc_exp2
		if(rst) begin
			exp2 <= 0;
		end else begin
			exp2 <= (en)?{BLOCK_SIZE{exp_in}} & BLOCK_MAX:exp2;
		end
	end

	always_ff @(posedge clk) begin : proc_data_in2
		if(rst) begin
			data_in2 <= 0;
		end else begin
			data_in2 <= (en)?data_in:data_in2;
		end
	end

	always_ff @(posedge clk) begin : proc_base_in2
		if(rst) begin
			base_in2 <= 0;
		end else begin
			base_in2 <= (en)?base_in:base_in2;
		end
	end

	always_ff @(posedge clk) begin : proc_skip_in2
		if(rst) begin
			skip_in2 <= 0;
		end else begin
			skip_in2 <= (en)?skip_in:skip_in2;
		end
	end

	always_ff @(posedge clk) begin : proc_exp_in2
		if(rst) begin
			exp_in2 <= 0;
		end else begin
			exp_in2 <= (en)?exp_in:exp_in2;
		end
	end

	//wire [BLOCK_SIZE-1:0] n_exp;
	//assign n_exp = {BLOCK_SIZE{(!exp_in)}} & BLOCK_MAX;
	/* verilator lint_off UNUSED */
	wire [BLOCK_SIZE:0] pb_sub2;
	assign pb_sub2 = (pullback2 - BLOCK_MAX);
	/* verilator lint_on UNUSED */

	always_comb begin : proc_data_out_next
		data_out_next = data_in2;
		if(!(data_base2 == exp2 || (data_base2 ^ BLOCK_MAX) == exp2 || (data_skip2 ^ BLOCK_MAX) == exp2)) begin
			if(pullback2 >= BLOCK_MAX) begin
				if(exp_in2) begin
					data_out_next[base_in2*BLOCK_SIZE+:BLOCK_SIZE]     = BLOCK_MAX;
					data_out_next[skip_in2*BLOCK_SIZE+:BLOCK_SIZE]     = 0;
					data_out_next[(base_in2+1)*BLOCK_SIZE+:BLOCK_SIZE] = pb_sub2[0 +: BLOCK_SIZE];
				end
				else begin
					data_out_next[(base_in2+1)*BLOCK_SIZE+:BLOCK_SIZE] = data_base_inc2;
					data_out_next[base_in2*BLOCK_SIZE+:BLOCK_SIZE] = pb_sub2[0 +: BLOCK_SIZE];
					data_out_next[skip_in2*BLOCK_SIZE+:BLOCK_SIZE] = BLOCK_MAX;
				end
			end
			else begin
				if(exp_in2) begin
					data_out_next[(base_in2+1)*BLOCK_SIZE+:BLOCK_SIZE] = data_base_inc2;
					data_out_next[base_in2*BLOCK_SIZE+:BLOCK_SIZE] = pullback2[0 +: BLOCK_SIZE];
					data_out_next[skip_in2*BLOCK_SIZE+:BLOCK_SIZE] = 0;
				end
				else begin
					data_out_next[(base_in2+1)*BLOCK_SIZE+:BLOCK_SIZE] = data_base_inc2;
					data_out_next[base_in2*BLOCK_SIZE+:BLOCK_SIZE] = 0;
					data_out_next[skip_in2*BLOCK_SIZE+:BLOCK_SIZE] = pullback2[0 +: BLOCK_SIZE];
				end
			end
		end
	end

	always_comb begin : proc_base_out_next
		if(exp_in2 == 0) begin
			base_out_next = base_in2 + 1;
		end
		else if(data_base2 == BLOCK_MAX) begin
			base_out_next = skip_in2;
		end
		else if ((data_base2 ^ BLOCK_MAX) == BLOCK_MAX) begin
			base_out_next = skip_in2;
		end
		else if ((data_skip2 ^ BLOCK_MAX) == BLOCK_MAX) begin
			base_out_next = skip_in2;
		end
		else begin
			if(pullback2 >= BLOCK_MAX) begin
				base_out_next = base_in2 + 1;
			end
			else begin
				base_out_next = base_in2;
			end
		end
	end

	//Note: in bin_pack.sv, this value is not used.  It is emulated through g_pipe+1
	always_comb begin : proc_skip_out_next
		skip_out_next = skip_in2 + 1;
	end

	always_comb begin : proc_exp_out_next
		if(data_base2 == exp2) begin
			exp_out_next = exp_in2;
		end
		else if ((data_base2 ^ BLOCK_MAX) == exp2) begin
			exp_out_next = !exp_in2;
		end
		else if ((data_skip2 ^ BLOCK_MAX) == exp2) begin
			exp_out_next = !exp_in2;
		end
		else begin
			if(pullback2 >= BLOCK_MAX) begin
				exp_out_next = exp_in2;
			end
			else begin
				exp_out_next = exp_in2;
			end
		end
	end

	always_ff @(posedge clk) begin : proc_data_out
		if(rst) begin
			data_out <= 0;
		end else begin
			data_out <= (en2)?data_out_next:data_out;
		end
	end

	always_ff @(posedge clk) begin : proc_base_out
		if(rst) begin
			base_out <= 0;
		end else begin
			base_out <= (en2)?base_out_next:base_out;
		end
	end

	always_ff @(posedge clk) begin : proc_skip_out
		if(rst) begin
			skip_out <= 0;
		end else begin
			skip_out <= (en2)?skip_out_next:skip_out;
		end
	end

	always_ff @(posedge clk) begin : proc_exp_out
		if(rst) begin
			exp_out <= 0;
		end else begin
			exp_out <= (en2)?exp_out_next:exp_out;
		end
	end

endmodule
