\RequirePackage{scrlfile}
\PreventPackageFromLoading{subfig}
\documentclass[a4paper,twocolumn,final,conference]{IEEEtran}

\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{soul}
\usepackage{array}
\usepackage{mathtools}
\usepackage{subcaption}
\usepackage{rotating}
\usepackage{adjustbox}
\usepackage{tikz}
\usepackage[europeanresistors]{circuitikz}
\usepackage{balance}
\usepackage{relsize}
\usetikzlibrary{math,calc,arrows.meta}
\usepackage{svg}
\usepackage[hidelinks]{hyperref}

%answer from Qrrbrbirlbel for https://tex.stackexchange.com/questions/134067/circuitikz-wire-kink-thingy-when-wires-cross
\tikzset{
	declare function={% in case of CVS which switches the arguments of atan2
	atan3(\a,\b)=ifthenelse(atan2(0,1)==90, atan2(\a,\b), atan2(\b,\a));},
	kinky cross radius/.initial=+.125cm,
	@kinky cross/.initial=+, kinky crosses/.is choice,
	kinky crosses/left/.style={@kinky cross=-},kinky crosses/right/.style={@kinky cross=+},
	kinky cross/.style args={(#1)--(#2)}{
	to path={
		let \p{@kc@}=($(\tikztotarget)-(\tikztostart)$),
			\n{@kc@}={atan3(\p{@kc@})+180} in
		-- ($(intersection of \tikztostart--{\tikztotarget} and #1--#2)!%
			 \pgfkeysvalueof{/tikz/kinky cross radius}!(\tikztostart)$)
		arc [ radius     =\pgfkeysvalueof{/tikz/kinky cross radius},
			start angle=\n{@kc@},
			delta angle=\pgfkeysvalueof{/tikz/@kinky cross}180 ]
		-- (\tikztotarget)}}}


\begin{document}

\title{A Long-Range Hardware Bubble Corrector Technique for Short-Pulse-Width and Multiple-Registration Encoders}
\author{
	\IEEEauthorblockN{Scott~Tancock}
	\IEEEauthorblockA{Department of Electrical\\
	and Electronic Engineering\\
	University of Bristol\\
	United Kingdom}
	\and \IEEEauthorblockN{John~Rarity}
	\IEEEauthorblockA{Department of Electrical\\
	and Electronic Engineering\\
	University of Bristol\\
	United Kingdom}
	\and \IEEEauthorblockN{Naim~Dahnoun}
	\IEEEauthorblockA{Department of Electrical\\
	and Electronic Engineering\\
	University of Bristol\\
	United Kingdom}
	}
\maketitle

\begin{abstract}
Bubble detection and correction logic is vital in modern data capture devices to solve bubbles in the output thermometer codes due to non-linearities in the scale causing negative bin widths.  Previous bubble correction techniques are either unsuitable for short pulse widths and multiple registration (ones-encoder) or have a very short range (all other methods).  In this paper, we propose a hardware technique to detect and correct bubbles up to the length of the pulse width while preserving position information using a hybrid between the ones-encoder and a single stage of a modified insertion sort.  This design was shown to meet timing on a Xilinx Artix-7 FPGA at 100~MHz or above using only 13\% of the device, demonstrating hardware-viability.  The design is also fully-pipelined to demonstrate high bandwidths.  The limitations of the algorithm are stated and some possible improvements are suggested.
\end{abstract}

\begin{IEEEkeywords}
Hardware, Field-Programmable Gate Array, Bubble Correction, Converters, Encoders, Thermometer Code, Population Counter
\end{IEEEkeywords}

\section{Introduction}
\IEEEPARstart{P}{rocess}, Voltage and Temperature (PVT) variations in modern Analog-to-Digital Converters (ADCs) and Time-to-Digital Converters (TDCs)  are increasing, mainly attributed to the shrinking process node and higher resolution of these devices.  This results in larger non-linearities in the output codes of these devices, eventually causing negative-width output bins (lower-valued codes that occur at a higher input value than higher-valued codes).  To correct for these disturbances, many bubble detection and correction techniques have been developed \cite{Xplore4529581,Xplore608492,Xplore852659,Xplore1414916,Xplore896906,Xplore6553400}.

However, these techniques all have a very limited scope.  The ones-encoder (also known as a population counter) method \cite{Xplore608492} is intolerant of short pulse widths or double registration, as it does not provide any information about the position of the edge, just the number of ones.  In a system where we can guarantee that the lowest bits will not transition on large input values (single-registration, long pulse width), this is equivalent to the position of the leading edge with all bubbles removed.  However, for techniques such as double-registration, the sliding scale technique (in some cases) and wave-union TDCs, the later introduction of 0s into the output code results in the ones-encoder failing to differentiate between ones at lower positions and ones at higher positions.

Techniques other than the ones-encoder such as the N-bit NAND technique \cite{Xplore852659}, multiplexer technique \cite{Xplore1414916}, Butterfly sorting technique \cite{Xplore896906} and Stepped-Up Tree Encoder (SUTE) \cite{Xplore6553400} are all very limited in their range and do not scale effectively with the size of the bubble.  This is suitable for simple converter architectures such as carry chains (in an FPGA TDC) \cite{Xplore8022888} and resistor ladders (in an ADC) \cite{Xplore4529581}, but less useful for more complicated architectures such as 2-D comparative encoders \cite{Xplore993038} or, as the authors discovered, in the case of DSP delay lines \cite{tancock5}.  These encoders may have prohibitively large bubbles (96 bins in the case of DSP delay lines) due to complex architectural forwarding techniques (e.g.\ carry look-ahead adders) for techniques other than the ones-encoder.

The authors used a ones-encoder in their previous paper on DSP delay lines \cite{tancock5}, but in order to implement the wave-union technique on the DSP delay lines, the authors needed a long-range bubble correction technique that preserved edge position.

To this end, we suggest a two-stage bubble-correction algorithm consisting of a local ones-encoder to produce partial codes and then a hybrid between a single stage of an insertion sort and a bin-packing algorithm which can separate the ones and zeros around the edge.

The rest of this paper will be structured as follows: Section~\ref{sec:algorithm} will describe the algorithm to be implemented on the FPGA, Section~\ref{sec:implementation} will describe the implementation details, including the pipelining of the core loop to achieve 100~MHz, zero-dead-time operation, Section~\ref{sec:results} will show the results of the algorithm operating on captured codes from our DSP delay line, Section~\ref{sec:limitations} will describe the limitations and extensibility of the algorithm and Section~\ref{sec:conclusion} will conclude the paper.

\section{Algorithm}
\label{sec:algorithm}
The proposed algorithm consists of two components: a local ones-encoder stage and a global bin-packing stage.  The local ones-encoder stage solves highly out-of-order codes within a defined range to produce monotonically-increasing minicodes, while the global bin-packing stage solves for the case of ones bleeding from one local encoder to the next.

\subsection*{Stage 1. The Local Ones Encoder}

\begin{figure}
	\resizebox{\columnwidth}{!}{
		\fbox{
			\input{fig_data_src}
		}
	}
	\caption{Data input from code generator to the ones-encoders then to the bin packer.}
	\label{fig:data_src}
\end{figure}

The first stage is to implement a ones-encoder that is no longer than half a pulse width.  Implementations of ones-encoders are already well documented, with perhaps the most common and efficient method being the Wallace-tree encoder \cite{Xplore4529581}, so they will not be discussed here.  For the authors, the natural delineation of ones-encoders came from the 48-bin boundary between DSP blocks, but any moderate size is acceptable.

A larger ones-encoder is preferable, as the second stage of the algorithm requires as many cycles latency as there are local ones-encoders.  However, if the ones-encoders contain more than half the bins in a minimum-sized pulse, there is no guarantee that there will be a full ones-encoder between two edges, which is required for correct operation of the algorithm, hence the maximum size limitation.  In the authors' work on DSP delay lines and the wave union technique, the size of the FSR pulse injector \cite{Xplore4775079} was chosen to be longer than three ones-encoders large.

\subsection*{Stage 2. The Global Bin-Packer}

\begin{figure*}
	\centering
	\resizebox{\textwidth}{!}{
		\fbox{
			\input{fig_bin_pack}
		}
	}
	\caption{Block diagram of the bin packer top level.}
	\label{fig:bin_pack}
\end{figure*}

The global bin packer algorithm is a hybrid between a bin packing algorithm and an insertion sort.  The kernel of the algorithm takes the data, two pointers (upper and lower) and the expected state of the bins immediately before the lower pointer, then decides on the best distribution of data between these bins (and one other `overflow' bin) in order to push ones and zeros towards the correct side of the edge.  It also decides on the correct next values of the pointers.

If this kernel were iterated $N^2-N$ times, with a reset of the pointers every $N$ iterations (where $N$ is the number of ones-encoders), then the algorithm would be guaranteed to compress a bubble of any size, in a similar fashion to an insertion or bubble sort.  However, to keep runtime and logic requirements sensible, we suggest only performing $N$ iterations, meaning the algorithm can only compress bubbles that span two ones-encoders.  This is because the algorithm cannot move in reverse, and so when the expected value is 1s the algorithm cannot pull ones more than 2 bins away to the current location.  With the fully pipelined kernel described later, this results in a runtime of $2N$ clock cycles.

Looking at the kernel in detail, we see that the action depends on the expected value of the bins just below the lower pointer.  If 0s are expected, then we are expecting a $0 \rightarrow 1$ transition in the future, so we will attempt to push ones forward.  Therefore, if the two codes pointed at by the upper and lower pointers have values X and Y respectively, we will attempt to put X+Y in upper code (originally contained Y).  If $X+Y$ is greater than the maximum output of a ones-encoder ($MAX$), then we will put the maximum value in the upper code and put $X+Y-MAX$ in the code below the upper code.  We set the lower code to 0 unless it is adjacent to the upper code and $X+Y > MAX$.

On the other hand, if the expected value of the bins below the lower pointer is 1, then we wish to pull ones back towards the transition (pushing 0s forward).  In this case, $min(X+Y, MAX)$ goes into the lower pointer, 0 goes into the upper pointer unless $X+Y > MAX$ and the codes are adjacent, and $X+Y-MAX$ goes into the bin above the lower bin if $X+Y > MAX$.

The expected value for the next iteration of the kernel is also calculated. If the expected value of the bins is 0 and the upper code is $MAX$, or the expected value of the bins is 1 and the upper code is 0, then the expected value is switched from 0 to 1 and 1 to 0 respectively, otherwise it remains the same.

As for the next locations to look at, we define that the upper pointer must increment by 1 each iteration of the kernel, resulting in a tightly-bounded runtime of $N$ iterations.  The lower pointer is dependent on the values of the codes.  If the upper code is 0 or MAX, the lower pointer skips forward to the current value of the upper pointer (one below the next value of the upper pointer).  If the expected value of the bins is 0s, the lower pointer will be successively emptying bins as it goes, so the lower pointer will increment by one each iteration.  If the expected value of the bins is 1s, then the lower pointer will stay at the current location to receive ones pushed back by the upper pointer, except if the code it points to `fills up' (reaches a value of $MAX$ and starts overflowing to the next bin), at which point it will move forward by 1.

\section{Implementation}
\label{sec:implementation}
The ones-encoder and bin-packer were implemented in hardware.  For simplicity, a binary tree adder was used for the ones-encoder (rather than the more efficient but more complicated Wallace-tree).  The bin-packer was implemented as two modules, the bin\_pack\_block module which implemented the kernel of the bin-packer, and the bin\_pack module which implemented the N-iteration loop in the form of an auto-generated pipeline of bin\_pack\_block modules.  This can be seen in Figure~\ref{fig:bin_pack}, where the bin\_pack\_block modules are denoted as `Kernel' and connections to the bin\_pack\_block module are shown in Figure~\ref{fig:kernel_pins}.

It can be seen that the bin\_pack\_block modules take in the data vector (data\_in), the upper and lower pointers (skip\_in and base\_in respectively), the expected value of bins (exp\_in), an enable signal, an active-high reset (not shown) and a clock signal.  The modules output the modified data vector (data\_out), the new values of the upper and lower pointers (skip\_out and base\_out) and the new expected value of the bins (exp\_out).

As the value of skip\_out is always one higher than skip\_in, the skip\_in value is hard-coded by the bin\_pack module for better logic synthesis.

The bin\_pack module manages the movement of data through the pipeline using the occupied, aux and en signals.  The occupied signal is a D-type Flip-Flop (DFF) which signals whether there is valid data at a particular position in the pipeline.  The en signal determines whether a particular pipeline stage forwards the data to the next stage, and is derived from the occupied and en values from the next stage.  The aux signal allows auxiliary data to be carried in lock-step with the data to be processed, such as channel or acquisition time information (in the authors' work, this was the value of the TDC coarse counter).

\begin{figure*}
	\resizebox{\textwidth}{!}{
		\fbox{
			\input{fig_do_sch}
		}
	}
	\caption{Schematic for the kernel, with extra pipeline registers.}
	\label{fig:do_sch}
\end{figure*}

The internals of the bin\_pack\_block module can be seen in Figure~\ref{fig:do_sch}.  The module can be roughly split into three parts: a data extraction part, a data selection part, and a data insertion part.

The extraction part is on the left, before the optional flip-flop stage. This part extracts data from the data\_in vector according to the values of base\_in and skip\_in.  If we call the data selected by base\_in `data\_base' and the data selected by skip\_ in `data\_skip', then the algorithm also calculates data\_base + data\_skip and data\_base + data\_skip - MAX (MAX = 48 in the authors' work).  It also calculates the expected value of the delay line before base\_in from the 1 bit signal exp\_in.

This data is then optionally registered by a stage of D-type Flip-Flops (DFFs).  Doing this cuts the critical path in half, resulting in significantly higher clock speeds (not quite double, as there is overhead from the flip-flop and net fanout).

The next part is the data selection part, where a number of multiplexers select the correct data to insert into the output data vector.  This corresponds to a number of if statements in the source code.  The conditions for the data selection are computed near the bottom diagram using the equality and less than operators, then some gates to combine the results.  The outputs of the gates serve as the select inputs to the multiplexers, with the data to be inserted on each branch of the condition as the data inputs.  Similar selections also occur for the exp\_in and base\_in signals.

The final part is data insertion.  Here, the data selected earlier is inserted into the output data vector, overwriting a group of 6 bits.  This is accomplished using the BMERGE block (Figure~\ref{fig:bmerge}), which takes an offset at the S input and some data at the I input.  The Q output vector has the bit values of I[5:0] at bit locations Q[S+5:S], while the rest of the bits are the same as the D input vector.

The outputs of the BMERGE blocks are cascaded and the final one is registered before leaving the module.  The base\_out and exp\_out signals simply register the selected data without any merging.  As skip\_out is always skip\_in + 1, the logic is very simple here.

By splitting the internals of the kernel into two stages, one which looks-up the required data from the input vector and calculates the required sums ($X+Y$ and $X+Y-MAX$), and another which selectively writes to the output vector, we were able to significantly reduce the latency and hence improve the clock frequency.  The source code of the implementation can be found at \cite{tancock2019lr}.

\section{In-Situ Results}
\label{sec:results}
The design was instantiated with typical parameters for the authors' use case: 24 codes, each 6 bits wide with a maximum value of 48 (due to each code relating to a DSP block).  This results in 144-bit input and output vectors.

With a single clock cycle kernel (the series of registers in the centre of Figure~\ref{fig:do_sch} removed), the design takes 11~ns cycle-to-cycle on an Artix-7 FPGA (with aggressive optimisation settings), meaning it would not reach the target 100~MHz clock frequency.  With a two-cycle kernel, the cycle-to-cycle latency was reduced to 7.668~ns, which could achieve a 130~MHz clock frequency if required.

The area utilisation on the XC7A200T chip used for this work was 13\% of the slice LUTs (Look-Up Tables) and 3.1\% of the slice FFs, which is not small, but given that the logic is outputting 144 bits of data at 100~MHz (14.4~Gbit/s), this is well above the speed of most interfaces, and so reducing the depth of the pipeline (requiring more dead-time) is entirely possible to reduce area utilisation.

Due to non-optimal packing of logic, the number of slices occupied is 21\% of the total.  However, if the FPGA was pressed for space, the implementation tools would fill the gaps with other logic or the design's floorplan would be more tightly compressed, at the expense of timing efficiency.

\balance
\section{Limitations and Extensibility}
\label{sec:limitations}
Our design does have some significant limitations.  One of the most significant is that there must always be at least one full ones-encoder between two edges, placing an upper limit on the size of the ones-encoders of half the pulse width (so that, regardless of the phase relationship between the pulse and ones-encoders, there will always be a full code).

Significant margin must also be added on top of this in order to ensure that the assorted ones and zeros around the edge do not spill into the guaranteed bin.  In the authors' application, nominally 3 codes (i.e. 3 DSP blocks) between edges was sufficient to ensure a full bin between each edge.

In addition, with only N iterations, the algorithm can only guarantee to deal with bubbles of two ones-encoders width.  If the bubble can extend further than this (i.e.\ split itself over 3+ consecutive codes), more iterations of the algorithm are required in order to deal with the $0 \rightarrow 1$ transition case.  $2N$ iterations are sufficient for bubbles split over 3 codes, $3N$ for 4 codes, etc.  Repeating the algorithm for more cycles is a sufficiently simple process.

In the future, it may be appropriate to investigate operating the algorithm in two directions (top to bottom, bottom to top), requiring $2N$ iterations.  We see that when pulling bins back ($1 \rightarrow 0$ transitions), the algorithm perfectly retrieves all 1s regardless of the bubble length.  When iterating in the other direction, $1 \rightarrow 0$ and $0 \rightarrow 1$ transitions swap, so these two directions should be sufficient to fix all bubbles, regardless of size.

This is somewhat similar to the bubble and insertion sort, where moving data from one end to the other is efficient, but moving data in the other direction is not.  Two-way bubble and insertion sorts solve this problem, and the same can be said here.

If the number of edges and maximum edge length were known in advance, then an alteration to the algorithm could be applied where it alternates between finding the next edge in the delay line and compressing the bubbles around that edge.  For very long delay lines or low numbers of edges (edges are relatively sparse, or all clustered in a small area), this could significantly improve the runtime of the algorithm, and is suggested for future work.

\section{Conclusion}
\label{sec:conclusion}
In this paper, we proposed a hardware bubble correction algorithm capable of dealing with very large bubbles in a priority code while maintaining position information, making it suitable for multiple registration and short pulse width applications.  This makes it ideal for implementing wave union TDCs with highly non-linear delay lines.

The algorithm was implemented on an Artix-7 200T FPGA and utilised 13\% of the LUTs and 3.1\% of the registers.  It achieved a worst-case cycle-to-cycle latency of 11~ns with a single-cycle kernel and 7.668~ns when fully pipelined (with a two-cycle kernel), allowing for a theoretical maximum frequency of 130~MHz.  This was performed with 24 codes, each 6 bits wide, with a maximum value per-code of 48, meaning it was capable of processing 144 bits per cycle at the target speed of 100~MHz (14.4~Gb/s) or higher.

We have highlighted the limitations of the algorithm, including the requirement for a full ones-encoder between each edge, which provides an upper bound for the size of a code.  This was shown to constrain the runtime and area of the algorithm.  Methods were suggested to improve the range of the bubble compression, if required.

\begin{figure}[t]
	\begin{subfigure}[b]{0.512\columnwidth}
		\centering
		\resizebox{\columnwidth}{!}{
			\fbox{
				\input{fig_bmerge}
			}
		}
		\caption{}
		\label{fig:bmerge}
	\end{subfigure}\hfill
	\begin{subfigure}[b]{0.488\columnwidth}
		\centering
		\resizebox{\columnwidth}{!}{
			\fbox{
				\input{fig_kernel_pins}
			}
		}
		\caption{}
		\label{fig:kernel_pins}
	\end{subfigure}
	\caption{(\subref{fig:bmerge}) Function of a BMERGE block. (\subref{fig:kernel_pins}) Pin arrangement on one of the Kernel blocks.}
	\label{fig:blocks}
\end{figure}
%\balance
\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,bubble-correct}

\end{document}
